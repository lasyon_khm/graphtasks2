package com.company.graph2;

import java.util.*;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        Set<Integer> hashSet = new HashSet<>();
        HashMap<Integer, Integer> answerMap = new HashMap<>();

        for (int i = 0; i < adjacencyMatrix.length; i++) {
            answerMap.put(i, Integer.MAX_VALUE);
        }
        answerMap.put(startIndex, 0);
        hashSet.add(startIndex);

        while (hashSet.size() != adjacencyMatrix.length) {

            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (!hashSet.contains(i) && i != startIndex && adjacencyMatrix[startIndex][i] > 0) {
                    answerMap.put(i, Math.min(answerMap.get(i), answerMap.get(startIndex) + adjacencyMatrix[startIndex][i]));

                }
            }
            int min = Integer.MAX_VALUE;
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (answerMap.get(i) < min && i != startIndex && !hashSet.contains(i)) {
                    min = answerMap.get(i);
                    startIndex = i;
                }
            }
            hashSet.add(startIndex);
        }
        return answerMap;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        Set<Integer> visitedList = new HashSet<>();
        TreeSet<Integer> sortedValues = new TreeSet<>();
        int startIndex = (int) (Math.random() * adjacencyMatrix.length);
        int result = 0;
        visitedList.add(startIndex);

        for (int i = 0; i < adjacencyMatrix.length; i++) {
            if (adjacencyMatrix[startIndex][i] != 0) {
                sortedValues.add(adjacencyMatrix[startIndex][i]);

            }
        }

        while (visitedList.size() != adjacencyMatrix.length) {
            int u = 0;
            int v = 0;
            int min = 0;
            if (!sortedValues.isEmpty()) {
                min = sortedValues.last();
            }

            for (Integer i : visitedList) {
                for (int j = 0; j < adjacencyMatrix.length; j++) {
                    if (!visitedList.contains(j) && adjacencyMatrix[i][j] != 0 && adjacencyMatrix[i][j] < min) {
                        u = i;
                        v = j;
                        min = adjacencyMatrix[i][j];
                    }
                    if (adjacencyMatrix[i][j] != 0 && (!visitedList.contains(j))) {
                        sortedValues.add(adjacencyMatrix[i][j]);
                    }
                }
            }
            sortedValues.pollFirst();
            visitedList.add(u);
            visitedList.add(v);

            result += min;
        }
        return result;
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        List<Edge> edges = MatrixToEdgeSet(adjacencyMatrix);

        Collections.sort(edges);

        int n = adjacencyMatrix.length;
        int result = 0;

        int[] sets = new int[n];
        for (int i = 0; i < n; i++) {
            sets[i] = i;
        }

        for (Edge e : edges) {
            if (isBridge(sets, e)) {
                result += e.getW();
                union(sets, e);
            }
        }

        return result;

    }

    public List<Edge> MatrixToEdgeSet(int[][] matrix) {
        ArrayList<Edge> edges = new ArrayList<>();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                int value = matrix[i][j];
                if (value != 0) {
                    int min = Math.min(i, j);
                    int max = Math.max(i, j);
                    Edge edge = new Edge(min, max, value);
                    if (!edges.contains(edge)) edges.add(new Edge(min, max, value));
                }
            }
        }
        return edges;
    }

    public boolean isBridge(int[] sets, Edge e) {
        int u = e.getU();
        int v = e.getV();

        return sets[u] != sets[v];
    }

    public void union(int[] sets, Edge e) {
        int u = e.getU();
        int v = e.getV();
        int uSet = sets[u];
        int vSet = sets[v];
        int max = Math.max(uSet, vSet);
        int min = Math.min(uSet, vSet);

        for (int i = 0; i < sets.length; i++) {
            if (sets[i] == max) sets[i] = min;
        }
    }
}
